# -*- coding: utf-8 -*-
# Authors:
#   Fjalar Sigurðarson (fjalar@vedur.is)
# Date:   Jun 2016


import glob, sys, os, os.path, time, json
import requests, hashlib


# Headers in this case are the first 16 bites of a file that define the file
headers = { '1f9d':'a .Z file', #standard (Lempel-Ziv-Welch) compression
            '312e':'a Rinex file',
            '1f8b':'a gzip file',
            '2440':'sbf septentrio file',
            '020f':'Trimble NetRS file',
            '0000':'Trimble NetR9 file',
            '4a50':'Topcon file',
            'gggg':'an unknown file type'}

for filename in glob.iglob('samples/*'):

    f = open(filename, "rb")
    #print filename
    try:
        # Read the first 2 bytes from the file 
        byte = f.read(2)
        # Encode it as hex... reads better.
        hex_line = byte.encode('hex')
        #print hex_line
        # Get full path for our file
        abs_path = os.path.abspath(filename)
        #abs_path = filename 

        # Empty files occurr. Let's not let it crash our code
        if not hex_line:
            hex_line = 'gggg'

        # Check if header pattern is in our dictionary and report
        try:
            if next(v for k,v in headers.items() if hex_line in k):
                #print "{0} is {1}".format(abs_path,headers[hex_line])

                # If the file has Z compression and is then probably a Rinex file
                if hex_line == '1f9d':
        
                    #1) parse file name and path
                    print abs_path

                    md5sum = hashlib.md5(abs_path).hexdigest()

                    marker = abs_path[-14:-10]
                    year = abs_path[-5:-3]
                    day_of_year = abs_path[-10:-6]
                    file_name = abs_path[-14:]
                    path = abs_path[:-14]
                    last_modified = time.ctime(os.path.getmtime(abs_path))
                    file_size = os.stat(abs_path).st_size
                    #print marker, year, day_of_year, path, file_name, md5sum, file_size, last_modified

                    #2) send info to service
                    body = json.dumps({
                        "station_marker": "ALCI",
                        "file_name": file_name,
                        "full_url":abs_path,
                        "local_path": path,
                        "data_center": "IMO",
                        "reference_date":"2017-06-10 00:00:00",
                        "published_date":last_modified,
                        "file_size": file_size,
                        "md5_checksum": md5sum
                        })

                    print "body: ", body

                    # Post the information
                    web_service_host = 'http://10.71.10.158:5000'
                    r = requests.post(web_service_host+'/gps/data/rinex', data = body)
        
                print "File posted {} Status code: {} Text: {}".format(file_name, r.status_code, r.text)

        except Exception as err:
            #print "{0} is a unknown file with header {1}".format(abs_path, hex_line)
            raise err
            
    finally:
        f.close()