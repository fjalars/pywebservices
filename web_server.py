# -*- coding: utf-8 -*-
# Authors: 
#   Fjalar Sigurðarson (fjalar@vedur.is)
# Date:   Nov 2016

import json
import sys
from httplib import NO_CONTENT, OK, NOT_FOUND, CONFLICT, BAD_REQUEST, INTERNAL_SERVER_ERROR, CREATED
import flask
from flask import Flask
from flask import request
from queries import Queries
from ConfigParser import SafeConfigParser # https://docs.python.org/2/library/configparser.html

app = Flask(__name__)

def parseConfig(type =""):
    '''
    Config parser that returns necessary parameters for the server to run
    '''

    try:
        configFile = "web_server.cfg"
        parser = SafeConfigParser()
        parser.read(configFile)
        #configs.append(parser)

        ## Server parameters ##
        if type == "server":
            app_debug = parser.get("Server", "app_debug")
            try:
                app_run = parser.get("Server", "app_run")
            except Exception as e:
                app_run = None
            return app_run, app_debug

        ## Database parameteres ##
        if type == "database":

	    # Pull database settings info from the config file,
            # store in a dict and return it	
            db_info = {'db_type':parser.get("Database", "db_type"), 
                       'username':parser.get("Database", "username"), 
                       'password':parser.get("Database", "password"),
                       'hostname':parser.get("Database", "hostname"), 
                       'db_name':parser.get("Database", "db_name")
                       }

            return db_info

    except Exception, e:
        print >> sys.stderr, "Error in parseConfig(): "
        print >> sys.stderr, e

## Instantiate a database conneciton
db_info = parseConfig("database")
queries = Queries(db_info)

###
### Services begin ###
###

@app.route('/', alias=True)
@app.route('/gps/', alias=True)
@app.route('/gps')
def hello():
    hello_message = '''<h2>Hello! I am your friendly Flask webserver built to function as a GPS Web Service</h2>
    <h4> Services currently available: </h4>
    <h4> <a href="{0}gps/station">{0}gps/station</a>To get a list of all GPS stations with a subset of metadata.</h4>
    <h4> <a href="{1}gps/station/test">{1}gps/station/test</a>To get all information for a certain station based on a marker.</h4>
    '''.format(request.url_root,request.url_root)

    return hello_message

@app.route('/gps/station', methods=['GET', 'POST'])
def station():

  #Preset variables
  status_code = NO_CONTENT
  result = ("<h1>{} - This service gave no results.</h1>Either the database is empty or necessary tables are missing data.".format(status_code),NO_CONTENT)

  ## Handle GET requests
  if request.method == 'GET':

    # 1) Argument handling

    ## Define and prese arg dict
    args = {'bbox':None, 'llrad':None, 'dates':None, 'item_type':None, 'model':None}

    ## Collect arguments to the arg dict
    for key in args:
      if request.args.get(key):
        args[key] = request.args.get(key)

    ## FOR TESTING PURPOSES: output arguments
    if args['bbox']:
      bbox = args['bbox'].split(';')
      print 'Bounding box:', bbox

    if args['llrad']:
      lat_lon_rad = args['llrad'].split(';')
      print "Lat: {} Lon: {} Radius: {} m ".format(lat_lon_rad[0], lat_lon_rad[1] , lat_lon_rad[2])

    if args['dates']:
      dates = args['dates'].split(';')
      print "Dates: from {} to {} ".format(dates[0],dates[1])

    if args['item_type']:
      item_type = args['item_type']
      print "Item type: {} ".format(item_type)

    if args['model']:
      model = args['model']
      print "Model: {}".format(model)

    # 2) Query database
    query_result, status_code = queries.station_list_get(args)

    # 3) Process the response 
    if status_code == OK:
      result = (flask.Response(query_result), status_code)
      result[0].headers['Content-type'] = 'application/json'

    elif status_code == NOT_FOUND:
      result = NOT_FOUND

  ## Handle POST requests
  elif request.method == 'POST':

    # 1) Collect JSON body 
    body = request.get_json(force=True)

    # 2) Query database
    query_result, status_code = queries.station_post(body)

    # Process and deliver the responce
    result = (flask.Response("<h1>{0}</h1>".format(query_result)),status_code)

  return result

@app.route('/gps/station/<station_identifier>', methods=['GET'])
def station_get_by_station_id(station_identifier):

  query_result, status_code = queries.station_single_get(station_identifier)

  # Process and deliver the responce
  result = (flask.Response("<h1>{0}</h1>".format(query_result)),status_code)
  result[0].headers['Content-type'] = 'application/json'

  return result

@app.route('/gps/sitelog', methods=['POST'])
def sitelog_post():

  # 1) Collect JSON body 
  body = request.get_json(force=True)

  # 2) Query database
  station_marker, station_id, status_code = queries.sitelog_post(body)

  # 3) Process the response
  if status_code == CREATED:
    #result = flask.Response(query_result)
    #result.headers['Content-type'] = 'application/json'
    result = (flask.Response("<h1> {} - New station {} was created with id = {}.</h1>".format(status_code, station_marker, station_id)),status_code)
    result[0].headers['Content-type'] = 'application/json'

  elif status_code == CONFLICT:
    result = (flask.Response("<h1> {} - Station {} is already in database. Already existing stations cannot be updated at present.</h1>".format(status_code, body['marker'].upper())),status_code)
    result[0].headers['Content-type'] = 'application/json'
  
  return result

@app.route('/gps/sitelog/<station_identifier>', methods=['GET'])
def sitelog_get_by_station_id(station_identifier):

  query_result, status_code = queries.sitelog_single_get(station_identifier)

  # Process and deliver the responce
  result = (flask.Response("<h1>{0}</h1>".format(query_result)),status_code)
  
  return result

@app.route('/gps/data/rinex', methods=['POST'])
def data_rinex_post():
 
  ## Formalize JSON body 
  body = request.get_json(force=True)

  ## Post data to database
  query_result, status_code = queries.data_rinex_post(body)

  # Process and deliver the responce
  result = (flask.Response("<h1>{0}</h1>".format(query_result)),status_code)
  #result[0].headers['Content-type'] = 'application/json'
  
  return result

@app.route('/gps/data/rinex/<station_identifier>', methods=['GET'])
def data_rinex_get_by_station_id(station_identifier):

  ## Query database
  query_result, status_code = queries.data_rinex_get(station_identifier)

  # Process and deliver the responce
  result = (flask.Response("<h1>{0}</h1>".format(query_result)),status_code)
  #result[0].headers['Content-type'] = 'application/json'
  
  return result

@app.route('/gps/data/rinex/<station_identifier>/<file_identifier>', methods=['GET'])
def data_rinex_get_by_station_and_file_id(station_identifier, file_identifier):

  ## Query database
  query_result, status_code = queries.data_rinex_single_get(station_identifier, file_identifier)
  
  # Process and deliver the responce
  result = (flask.Response("<h1>{0}</h1>".format(query_result)),status_code)
  
  return result

if __name__ == "__main__":
   # Run this version of app.run while devloping. Only localhost/127.0.0.1 can access the server
   #app.run()

   # Run this version of app.run to expose the server to the internal network
   app_run, app_debug = parseConfig("server")
   app.run(host=app_run, debug=app_debug)
