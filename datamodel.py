# -*- coding: utf-8 -*-
# Authors:
#   Fjalar Sigurðarson (fjalar@vedur.is)
# Date:   Nov 2016

'''
This is the module doc string
'''

## SqlAlchemy imports

from sqlalchemy import Integer, String, Text, Column, DateTime, ForeignKey, Numeric, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.associationproxy import association_proxy

## Declarations

Base = declarative_base()

## Data Models ##

class Station(Base):
    '''This is a doc string'''

    __tablename__ = 'station'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    marker = Column(String)
    description = Column(Text)
    date_from = Column(DateTime())
    date_to = Column(DateTime())
    id_station_type = Column(Integer, ForeignKey('station_type.id'))
    id_location = Column(Integer, ForeignKey('location.id'))
    id_monument = Column(Integer, ForeignKey('monument.id'))
    id_geological = Column(Integer, ForeignKey('geological.id'))
    iers_domes = Column(Text)
    cpd_num = Column(Text)
    antenna_num = Column(Text)
    receiver_num = Column(Text)
    country_code = Column(Text)
    comment = Column(Text)

    items = relationship("StationItem", back_populates="station")
    contacts = relationship("StationContact", back_populates="station")
    networks = relationship("StationNetwork", back_populates="station")

    def __repr__(self):
        return "<Station(id='%s', marker='%s', description='%s')>" % (self.id, self.marker, self.description)

class StationContact(Base):
    '''This is a doc string'''
    __tablename__ = 'station_contact'
    id = Column(Integer, primary_key=True)
    id_station = Column(Integer, ForeignKey('station.id'))
    id_contact = Column(Integer, ForeignKey('contact.id'))
    role = Column(Text)

    station = relationship('Station', back_populates='contacts')
    contact = relationship('Contact', back_populates='stations')

class Contact(Base):
    '''This is a doc string'''

    __tablename__ = 'contact'
    id = Column(Integer, primary_key=True)
    name = Column(Text)
    title = Column(Text)
    email = Column(Text)
    phone = Column(Text)
    gsm = Column(Text)
    comment = Column(Text)
    role = Column(Text)
    id_agency = Column(Integer, ForeignKey('agency.id'))
    stations = relationship("StationContact", back_populates="contact")
    #agencies = relationship("Agency", back_populates="contact")

class Agency(Base):
    '''This is a doc string'''

    __tablename__ = 'agency'
    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    abbreviation = Column(String(50))
    address = Column(Text)
    www = Column(Text)
    infos = Column(Text)

    #contacts = relationship("Contact", back_populates="agency")

class Location(Base):
    '''This is a doc string'''

    __tablename__ = 'location'
    id = Column(Integer, primary_key=True)
    description = Column(Text)
    id_coordinates = Column(Integer, ForeignKey('coordinates.id') )
    id_tectonic = Column(Integer, ForeignKey('tectonic.id'))
    id_city = Column(Integer, ForeignKey('city.id'))

class City(Base):
    '''This is a doc string'''

    __tablename__ = 'city'
    id = Column(Integer, primary_key=True)
    name = Column(Text)
    id_state = Column(Integer, ForeignKey('state.id'))

class State(Base):
    '''This is a doc string'''

    __tablename__ = 'state'
    id = Column(Integer, primary_key=True)
    name = Column(Text)
    id_country = Column(Integer, ForeignKey('country.id'))

class Country(Base):
    '''This is a doc string'''

    __tablename__ = 'country'
    id = Column(Integer, primary_key=True)
    name = Column(Text)
    iso_code = Column(Text)

class Bedrock(Base):
    '''This is a doc string'''

    __tablename__ = 'bedrock'
    id = Column(Integer, primary_key=True)
    condition = Column(Text)
    type = Column(Text)

    def __repr__(self):
        return "<Bedrock(id='%s', condition='%s', type='%s')>" % (self.id, self.condition, self.type)

class Monument(Base):
    '''This is a doc string'''

    __tablename__ = 'monument'
    id = Column(Integer, primary_key=True)
    description = Column(Text)
    inscription = Column(Text)
    height = Column(Numeric)
    foundation = Column(Text)
    foundation_depth = Column(Numeric)

    def __repr__(self):
        return "<Monument(id='%s', descripton='%s', inscription='%s, height='%s, foundation='%s, foundation_depth='%s')>" % (self.id, self.description, self.inscription, self.height, self.foundation, self.foundation_depth)

class Geological(Base):
    '''This is a doc string'''

    __tablename__ = 'geological'
    id = Column(Integer, primary_key=True)
    characteristic = Column(Text)
    fracture_spacing = Column(Text)
    fault_zone = Column(Text)
    distance_to_fault = Column(Text)
    id_bedrock = Column(Integer, ForeignKey('bedrock.id'))

    def __repr__(self):
        return "<Geological(id='%s', characteristic='%s', fracture_spacing='%s, fault_zone='%s', distance_to_fault='%s', id_bedrock='%s')>" % (self.id, 
                                                        self.characteristic, 
                                                        self.fracture_spacing,
                                                        self.fault_zone,
                                                        self.distance_to_fault,
                                                        self.id_bedrock)

class Tectonic(Base):
    '''This is a doc string'''

    __tablename__ = 'tectonic'
    id = Column(Integer, primary_key=True)
    plate_name = Column(Text)

    def __repr__(self):
        return "<Tectonic(id='%s', name='%s')>" % (self.id, self.name)

class Coordinates(Base):
    '''This is a doc string'''

    __tablename__ = 'coordinates'
    id = Column(Integer, primary_key=True)
    x = Column(Numeric)
    y = Column(Numeric)
    z = Column(Numeric)
    lat = Column(Numeric)
    lon = Column(Numeric)
    altitude = Column(Numeric)


    def __repr__(self):
        return "<Coordinates(id='%s', x='%s', y='%s', z='%s', lat='%s', lon='%s', altitude='%s')>" % (self.id, self.x, self.y, self.z, self.lat, self.lon, self.altitude)

class StationNetwork(Base):
    '''This is a doc string'''

    __tablename__ = 'station_network'
    id = Column(Integer, primary_key=True)
    id_station = Column(Integer, ForeignKey('station.id'))
    id_network = Column(Integer, ForeignKey('network.id'))
    network = relationship("Network", back_populates="stations")
    station = relationship("Station", back_populates="networks")

class Network(Base):
    '''This is a doc string'''

    __tablename__ = 'network'
    id = Column(Integer, primary_key=True)
    name = Column(Text)
    id_contact = Column(Integer, ForeignKey('contact.id'))
    stations = relationship("StationNetwork", back_populates="network")

class StationType(Base):
    '''This is a doc string'''

    __tablename__ = 'station_type'
    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    type = Column(String(50))

class LocalTie(Base):
    '''This is a doc string'''

    __tablename__ = 'local_ties'
    id = Column(Integer, primary_key=True)
    id_station = Column(Integer, ForeignKey('station.id'))
    name = Column(String(50))
    usage = Column(String(50))
    cpd_num = Column(String(10))
    dx = Column(Numeric)
    dy = Column(Numeric)
    dz = Column(Numeric)
    accuracy = Column(Numeric)
    survey_method = Column(Text)
    date_at = Column(DateTime())
    comment = Column(Text)

class InstrumentColocation(Base):
    ''' This is a doc string '''

    __tablename__ = 'instrument_collocation'
    id = Column(Integer, primary_key=True)
    id_station = Column(Integer, ForeignKey('station.id'))
    type = Column(String(50))
    status = Column(Text)
    date_from = Column(DateTime())
    date_to = Column(DateTime())
    comment = Column(Text)

class StationItem(Base):
    '''This is a doc string'''

    __tablename__ = 'station_item'
    id = Column(Integer, primary_key=True)
    id_station = Column(Integer, ForeignKey('station.id'))
    id_item = Column(Integer, ForeignKey('item.id'))
    item = relationship("Item", back_populates="stations")
    station = relationship("Station", back_populates="items")
    date_from = Column(DateTime())
    date_to = Column(DateTime())    

class Item(Base):
    '''This is a doc string'''

    __tablename__ = 'item'
    id = Column(Integer, primary_key=True)
    id_item_type = Column(Integer, ForeignKey('item_type.id'))
    item = relationship("ItemType")
    item_type = association_proxy('item', 'name')
    attributes = relationship("ItemAttribute")
    stations = relationship("StationItem", back_populates="item")

class ItemType(Base):
    '''This is a doc string'''

    __tablename__ = 'item_type'
    id = Column(Integer, primary_key=True)
    name = Column(String(50))

class ItemAttribute(Base):
    '''This is a doc string'''

    __tablename__ = 'item_attribute'
    id = Column(Integer, primary_key=True)
    id_item = Column(Integer, ForeignKey('item.id'))
    id_attribute = Column(Integer, ForeignKey('attribute.id'))
    value_varchar = Column(String(50))
    date_from = Column(DateTime())
    date_to = Column(DateTime())
    att = relationship("Attribute")
    name = association_proxy('att', 'name')

class Attribute(Base):
    '''This is a doc string'''

    __tablename__ = 'attribute'
    id = Column(Integer, primary_key=True)
    name = Column(String(50))

class RinexFile(Base):
    '''This is a doc string'''

    __tablename__ = 'rinex_file'
    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    id_station = Column(Integer, ForeignKey('station.id'))
    id_data_center = Column(Integer, ForeignKey('data_center.id'))
    file_size = Column(Integer)
    id_file_type = Column(Integer, ForeignKey('file_type.id'))
    relative_path = Column(String(100))
    reference_date = Column(DateTime())
    published_date = Column(DateTime())
    creation_date = Column(DateTime())
    revision_date = Column(DateTime())
    md5checksum = Column(String(40))
    md5uncompressed = Column(String(40))
    status = Column(Integer)

class FileType(Base):
    '''This is a doc string'''

    __tablename__ = 'file_type'
    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    format = Column(String(15))
    sampling_window = Column(String(15))
    sampling_frequency = Column(String(15))

class DataCenter(Base):
    '''This is a doc string'''

    __tablename__ = 'data_center'
    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    url = Column(String(200))
    access_condition = Column(String(200))
    acronym = Column(String(100))
    hostname = Column(String(200))
    id_agency = Column(Integer, ForeignKey('agency.id'))
