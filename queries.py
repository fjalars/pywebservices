# -*- coding: utf-8 -*-
# Authors:
#   Fjalar Sigurðarson (fjalar@vedur.is)
# Date:   Nov 2016

'''
This is the module doc string
'''

## General imports

import json
import logging
import logging.handlers
import decimal
from contextlib import closing
from datetime import datetime
from httplib import NO_CONTENT, OK, NOT_FOUND, CONFLICT, BAD_REQUEST, INTERNAL_SERVER_ERROR, CREATED

## SqlAlchemy imports
#from sqlalchemy import create_engine
from sqlalchemy import *
from sqlalchemy.orm import sessionmaker

## Data model imports
from datamodel import Station, Contact, StationContact, StationNetwork,\
                        Location, Agency, City, State, Country, Network, \
                        Bedrock, Geological, Tectonic, StationType, Coordinates, \
                        Monument, LocalTie, InstrumentColocation, ItemType, Attribute, RinexFile, \
                        FileType, DataCenter


## Support classes ##

class JSONCustomEncoder(json.JSONEncoder):
    """ Custom JSON encoder to handle datetime objects
    """
    def default(self, obj):

        if isinstance(obj, datetime):        
            return obj.strftime('%Y-%m-%d %H:%M:%S')

        if isinstance(obj, decimal.Decimal):
            return float(obj)

        return json.JSONEncoder.default(self, obj)

## Main class ##

class Queries(object):
    '''
    This is a doc string
    '''
    
    ### Init ###

    def __init__(self, db_info):
        '''
        This is a doc string
        '''

        ## -------------------------------------------------- ##
        ## 1) Set up database connection and bind to session  ##
        ## -------------------------------------------------- ##

        # FIXME: This needs to be moved to a config file.
        #db_info = {'db_type':'postgresql', 'user':'gps', 'password':'gps',
        #           'hostname':'localhost', 'db_name':'gps'}

        engine = create_engine('{0}://{1}:{2}@{3}:5432/{4}'.format(db_info['db_type'],
                                                                   db_info['username'],
                                                                   db_info['password'],
                                                                   db_info['hostname'],
                                                                   db_info['db_name']))
        self.session = sessionmaker(bind=engine)


        ## ----------------- ##
        ## 2) Define logger  ##
        ## ----------------- ##

        # create logger with current application
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)

        # create file handler which logs even debug messages
        f_handler = logging.handlers.RotatingFileHandler('queries.log', maxBytes=20, backupCount=0)
        f_handler.setLevel(logging.DEBUG)

        # create console handler with a higher log level
        c_handler = logging.StreamHandler()
        c_handler.setLevel(logging.ERROR)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(levelname)s: %(message)s',
                                       datefmt='%m/%d/%y %H:%M')
        # create formatter and add it to the handlers
        f_handler.setFormatter(formatter)
        c_handler.setFormatter(formatter)

        # add the handlers to the logger
        self.logger.addHandler(f_handler)
        self.logger.addHandler(c_handler)

        self.logger.info("Logger has been defined")
        self.logger.info("Queries object instantiated")

    ### Services ###

    def station_list_get(self,args):
        '''
        This is a doc string
        '''

        self.logger.info('station_list() was called')

        ## Preset result
        result = None
        query_list = None
        status_code = NO_CONTENT
        response_list = []

        with closing(self.session()) as session:

            ### Execute query ###
            self.logger.info('Just before the query...')
            
            query_list = session.query(Station, Agency, Coordinates,\
                            City, State, Country, Network)\
                    .join(StationContact)\
                    .join(Contact)\
                    .join(Agency)\
                    .join(Location)\
                    .join(Coordinates)\
                    .join(City)\
                    .join(State)\
                    .join(Country)\
                    .join(StationNetwork)\
                    .join(Network)\
                    .all()


             
            if query_list:

                ### Define a dict to collect the dictified models
                model_dicts = {'agency':{'name':None},
                               'station':{'id':None, 'marker':None, 'name':None, 'date_from':None,'date_to':None},
                               'country':{'name':None},
                               'city':{'name':None},
                               'state':{'name':None},
                               'network':{'name':None},
                               'coordinates':{'lat':None,'lon':None,'altitude':None}
                            }
                

                ### Loop through each collection of tables for every station
                for query in query_list:

                    ### Warp the model object to a dict
                    for query_object in query:
                        # FIXME: why this is needed has to be explained. I have a theory...
                        try:
                            query_dict = query_object.__dict__
                            query_dict.pop('_sa_instance_state')
                        except:
                            pass

                        model_dicts[query_object.__tablename__] = query_dict

                    ### Construct response ###
                    response = {
                        'agency': {
                            'name': model_dicts['agency']['name']
                        },
                        'id' : model_dicts['station']['id'],
                        'marker': model_dicts['station']['marker'],
                        'name': model_dicts['station']['name'],
                        'location': {
                            'country': {
                                'name': model_dicts['country']['name']
                            },
                            'city': {
                                'name': model_dicts['city']['name']
                            },
                            'coordinates': {
                                'altitude': model_dicts['coordinates']['altitude'],
                                'lon': model_dicts['coordinates']['lon'],
                                'lat': model_dicts['coordinates']['lat']
                            },
                            'state': {
                                'name': model_dicts['state']['name']
                            }
                        },
                        'date_to': model_dicts['station']['date_to'],
                        'date_from': model_dicts['station']['date_from'],
                        'station_networks': [
                            {
                                'network': {
                                    'name': model_dicts['network']['name']
                                }
                            }
                        ]
                    }

                    response_list.append(response)

            else:
                # No result - no content
                status_code = NO_CONTENT
                result = None


        #print response_list
        result = json.dumps(response_list, cls=JSONCustomEncoder)
        status_code = OK

        return result, status_code

    def station_single_get(self, marker_id=None):
        '''
        This is a doc string
        '''

        ## Preset result
        result = None
        status_code = NO_CONTENT

        self.logger.info('station_single() was called with marker_id: {}'.format(marker_id))

        id_station = None
        marker = None
        query_list = None
        response_list = []

        #Station identifier from http-input parameters
        if marker_id:
            if marker_id.isdigit():
                self.logger.info('marker_id is TYPE "digit"')
                id_station = int(marker_id)
            else:
                self.logger.info('marker_id is TYPE "marker"')
                marker = marker_id.upper()
        else:
            self.logger.info('marker_id: {} -> value is missing'.format(marker_id))


        with closing(self.session()) as session:

            ### Execute query ###
    
            query_list = session.query(Station, Agency, Coordinates,\
                                City, State, Country, Network)\
                                .join(StationContact)\
                                .join(Contact)\
                                .join(Agency)\
                                .join(Location)\
                                .join(Coordinates)\
                                .join(City)\
                                .join(State)\
                                .join(Country)\
                                .join(StationNetwork)\
                                .join(Network)\
                                .filter(or_(Station.marker == marker, Station.id == id_station))\
                                .all()

            if query_list:

                self.logger.info('Length of query result list: {0}'.format(len(query_list)))

                ### Define a dict to collect the dictified models
                model_dicts = {'agency':{'name':None},
                               'station':{'id':None, 'marker':None, 'name':None, 'date_from':None,'date_to':None},
                               'country':{'name':None},
                               'city':{'name':None},
                               'state':{'name':None},
                               'network':{'name':None},
                               'coordinates':{'lat':None,'lon':None,'altitude':None}
                            }
                

                ### Loop through each collection of tables for every station
                for query in query_list:

                    ### Warp the model object to a dict
                    for query_object in query:
                        # FIXME: why this is needed has to be explained. I have a theory...
                        try:
                            query_dict = query_object.__dict__
                            query_dict.pop('_sa_instance_state')
                        except:
                            pass

                        model_dicts[query_object.__tablename__] = query_dict

                    ### Construct response ###
                    response = {
                        'agency': {
                            'name': model_dicts['agency']['name']
                        },
                        'id' : model_dicts['station']['id'],
                        'marker': model_dicts['station']['marker'],
                        'name': model_dicts['station']['name'],
                        'location': {
                            'country': {
                                'name': model_dicts['country']['name']
                            },
                            'city': {
                                'name': model_dicts['city']['name']
                            },
                            'coordinates': {
                                'altitude': model_dicts['coordinates']['altitude'],
                                'lon': model_dicts['coordinates']['lon'],
                                'lat': model_dicts['coordinates']['lat']
                            },
                            'state': {
                                'name': model_dicts['state']['name']
                            }
                        },
                        'date_to': model_dicts['station']['date_to'],
                        'date_from': model_dicts['station']['date_from'],
                        'station_networks': [
                            {
                                'network': {
                                    'name': model_dicts['network']['name']
                                }
                            }
                        ]
                    }

                    response_list.append(response)

            else:
                # No result - no content
                status_code = NO_CONTENT
                result = None


        #print response_list
        result = json.dumps(response_list, cls=JSONCustomEncoder)
        status_code = OK

        return result, status_code

    def station_post(self, body):

        status_code = OK
        result = "Service has not been implemented"

        return result, status_code

    def sitelog_single_get(self, marker_id=None):

        status_code = OK
        result = "Service has not been implemented"

        return result, status_code

    def sitelog_post(self, body):
        '''
        This is a doc string
        '''

        ## Preset result
        result = None
        query = None
        status_code = NO_CONTENT
        station_marker = None
        station_id = None

        self.logger.info('station_post() was called')
        self.logger.info('Station marker: {}'.format(body['marker']))

        # Check first if station already exists in the database.
        with closing(self.session()) as session:

            # Check for station existens in the database and change the status_code accordingly.
            station_id = session.query(Station.id)\
                            .filter(Station.marker == body['marker'].upper())\
                            .one_or_none()

            if station_id:

                self.logger.info('station {} was found in the database.'.format(body['marker']))
                self.logger.info('Method to compare new Sitelog for current station is needed here...')
                status_code = CONFLICT

                ## TODO:
                # Currently nothing is done if station marker is found in database
                # but there needs to be implemented a process to compare the station config
                # in the database and the sitelog given to the servies, for update purposes.

        # If the status_code is unchanged, then no station was found and
        # we can create one from the sitelog.
        if status_code == NO_CONTENT:

            self.logger.info('station {} was NOT found in the database. Creating now...'.format(body['marker']))

            with closing(self.session()) as session:

                # 1 #
                bedrock_id = self.insert_bedrock(body, session)
                geological_id = self.insert_geological(body, session, bedrock_id)

                # 2 #
                country_id = self.insert_country(body, session)
                state_id = self.insert_state(body, session, country_id)
                city_id = self.insert_city(body, session, state_id)
                tectonic_id = self.insert_tectonic(body, session)
                coordinates_id = self.insert_coordinates(body, session)
                monument_id = self.insert_monument(body, session)
                location_id = self.insert_location(body, session, tectonic_id, city_id, coordinates_id)

                station_type_id = self.insert_station_type(body,session)

                # 3 #

                contact_id_list = []

                for contact in body['station_contacts']:
                    agency_id = self.insert_agency(contact, session)
                    contact_id = self.insert_contact(contact, session, agency_id) # list of ids
                    contact_id_list.append(contact_id)
                
                #network_id_list = self.insert_network(body, session, contact_id) # list of ids
                # TODO: Contact for network is not provided in sitelogs...
                contact_id = 'Not a number and never used...'
                network_id_list = self.insert_network(body, session, contact_id) # list of ids

                # Effect - Condition
                #effect_id = self.insert_effect(body,session)

                # 4 #
                station_marker, station_id = self.insert_station(body, session, station_type_id,
                                                                       location_id, geological_id,
                                                                       monument_id)

                ### Post station creation ###

                # 5 #

                self.insert_local_tie(body, session, station_id)
                self.insert_instrument_collocation(body,session,station_id)

                #Inserts will be added later...
                #self.insert_condition(body,session,station_id)
                #self.insert_log(body, session, station_id)

                #self.insert_item(body, session)
                #self.insert_item_attribute(body, session, id_item)

                self.insert_station_contact(session, station_id, contact_id_list)
                self.insert_station_network(session, station_id, network_id_list)

            status_code = CREATED

        return station_marker, station_id, status_code

    def data_rinex_post(self, body):

        self.logger.info('data_post() was called')
        rinex_file = RinexFile()
        rinex_file_id = None
        result = None
        query = None
        status_code = NO_CONTENT
        station_marker = body['station_marker']
        station_id = None
        status = "No status yet"

        ##
        data_center = None

        self.logger.info('Variables have been preset...')

        ### 2 ### Check if geological description exists and create one if not.
        with closing(self.session()) as session:

            # Check if the station already exists in the DB
            station_id = session.query(Station.id)\
                            .filter(Station.marker == station_marker)\
                            .one_or_none()

            # Find Data Center ID
            data_center_id = session.query(DataCenter.id)\
                                .filter(DataCenter.acronym == body['data_center'])\
                                .one_or_none()

            ##TODO: Implement better file_type table
            ## for now, there is only one record in the table for 'N/A'
            #file_type_id = session.query(FileType.id)\
            #                    .filter(and_(FileType.format == "RINEX2", FileType.sampling_window == "daily", FileType.sampling_frequency == "30s"))\
            #                    .one_or_none()            
            file_type_id = 2

            # File status is set here as default 1 until more information on this have been gained
            file_status = 1

            # If there is a station with this station_marker in the DB, continue
            if station_id:

                # Check if the file already exsists in the DB
                rinex_file_id = session.query(RinexFile.id)\
                                    .filter(and_(RinexFile.name == body['file_name'],RinexFile.md5checksum == body['md5_checksum']))\
                                    .one_or_none()


                if not rinex_file_id:

                    station_id = station_id[0]
                    self.logger.info('Station id ({0}) for station "{1}" was found.'.format(station_id, station_marker))
                    # The next line is a hack-fix. For some reason the query returns a tuple but
                    # not just the value - if there is a result is should only be one and integer.

                    rinex_file.name = body['file_name']
                    rinex_file.id_station = station_id
                    rinex_file.id_data_center = data_center_id
                    rinex_file.id_file_type = file_type_id
                    rinex_file.file_size = body['file_size']
                    rinex_file.relative_path = body['local_path']
                    rinex_file.reference_date = body['reference_date']
                    rinex_file.published_date = body['published_date']
                    rinex_file.md5checksum = body['md5_checksum']
                    rinex_file.status = file_status
                    session.add(rinex_file)
                    session.flush()

                    # Commit new file to the database
                    session.commit()

                    # Get the id for the new rinex file.
                    rinex_file_id = rinex_file.id

                    # Update status code
                    status_code = CREATED

                    # Define return status message
                    status = 'Entry was added and got id {0}'.format(rinex_file.id)
                else:
                    status = 'Entry is already in the database with id {0}'.format(rinex_file_id[0])
                    status_code = CONFLICT

                self.logger.info(status)

            else:
                # Define return status message
                status = 'Station with marker "{0}" was not found. Station must be registered in the database before files can be indexed.'.format(station_marker)
                self.logger.info(status)

                # Update status code
                status_code = CONFLICT
                

        return status, status_code

    def data_rinex_get(self, marker_id=None):

        status_code = OK
        result = "Service has not been implemented"

        return result, status_code 

    def data_rinex_single_get(self, marker_id=None, file_id=None):

        status_code = OK
        result = "Service has not been implemented"

        return result, status_code

    ### Database functions ###

    def insert_bedrock(self, body, session):

        try:

            #### Insert Bedrock info ####
            #self.logger.info('Checking bedrock information.')
            bedrock = Bedrock()
            bedrock_id = None

            ### 1 ### Fill N/A in for none
            if body['geological']['bedrock']['condition']:
                bedrock_condition = body['geological']['bedrock']['condition'].lower()
            else:
                bedrock_condition = 'None'

            if body['geological']['bedrock']['type']:
                bedrock_type = body['geological']['bedrock']['type'].lower()
            else:
                bedrock_type = 'None'

            ### 2 ### Check if bedrock description exists and create one if not.
            bedrock_id = session.query(Bedrock.id)\
                              .filter(and_(Bedrock.condition == bedrock_condition, Bedrock.type == bedrock_type))\
                              .one_or_none()

            if bedrock_id:
                bedrock_id = bedrock_id[0]
                self.logger.info('Bedrock information was found with id: {}'.format(bedrock_id))
                # The next line is a hack-fix. For some reason the query returns a tuple but
                # not just the value - if there is a result is should only be one and integer.
            else:
                # Description does not exist so it will be created in db.
                self.logger.info('Bedrock information not found.. Creating one now.')
                bedrock.condition = bedrock_condition
                bedrock.type = bedrock_type
                session.add(bedrock)
                session.flush()

                # Get the id for the new bedrock description. Will be stored in the geological table.
                bedrock_id = bedrock.id
                self.logger.info('Bedrock id: {}'.format(bedrock.id))
                session.commit()

            return bedrock_id

        except Exception as e:
            session.rollback()
            raise e

    def insert_geological(self, body, session, bedrock_id):

        try:
            #### Insert Geological info ####
            self.logger.info('Checking geological information')
            geological = Geological()
            geological_id = None

            ### 1 ### Fill N/A in for none
            if body['geological']['characteristic']:
                geological_characteristic = body['geological']['characteristic'].lower()
            else:
                geological_characteristic = 'None'

            if body['geological']['fracture_spacing']:
                geological_fracture_spacing = body['geological']['fracture_spacing'].lower()
            else:
                geological_fracture_spacing = 'None'

            if body['geological']['fault_zone']:
                geological_fault_zone = body['geological']['fault_zone'].lower()
            else:
                geological_fault_zone = 'None'

            if body['geological']['distance_to_fault']:
                geological_distance_to_fault = body['geological']['distance_to_fault'].lower()
            else:
                geological_distance_to_fault = 'None'

            ### 2 ### Check if geological description exists and create one if not.
            geological_id = session.query(Geological.id)\
                          .filter(and_(Geological.characteristic == geological_characteristic,
                            Geological.fracture_spacing == geological_fracture_spacing,
                            Geological.fault_zone == geological_fault_zone,
                            Geological.distance_to_fault == geological_distance_to_fault))\
                          .one_or_none()

            if geological_id:
                geological_id = geological_id[0]
                self.logger.info('Geological description was found: {}'.format(geological_id))
                # The next line is a hack-fix. For some reason the query returns a tuple but
                # not just the value - if there is a result is should only be one and integer.
            else:
                # Description does not exist so it will be created in db.
                self.logger.info('Geological description not found: {}'.format(geological_id))
                geological.id_bedrock = bedrock_id
                geological.characteristic = geological_characteristic
                geological.fracture_spacing = geological_fracture_spacing
                geological.fault_zone = geological_fault_zone
                geological.distance_to_fault = geological_distance_to_fault
                session.add(geological)
                session.flush()

                # Get the id for the new geological description. Will be stored in the station table.
                geological_id = geological.id
                self.logger.info('Geological id: {}'.format(geological.id))
                session.commit()

            return geological_id

        except Exception as e:
            session.rollback()
            raise e

    def insert_country(self, body, session):

        try:
            #### Insert Bedrock info ####
            #self.logger.info('Checking bedrock information.')
            country = Country()
            country_id = None

            ### 1 ### Fill N/A in for none
            if body['location']['country']:
                country_name = body['location']['country'].title()
                self.logger.info('Country: {}'.format(country_name))
            else:
                country_name = 'None'

            # TODO: ISO code not provided via sitelog - Figure out how to manage countries
            country_iso_code = 'None'

            ### 2 ### Check if country description exists and create one if not.
            country_id = session.query(Country.id)\
                              .filter(Country.name == country_name)\
                              .one_or_none()

            if country_id:
                country_id = country_id[0]
                self.logger.info('Country description was found: {} / {}'.format(country_id,country_name))
                # The next line is a hack-fix. For some reason the query returns a tuple but
                # not just the value - if there is a result is should only be one and integer.

            else:
                # Description does not exist so it will be created in db.
                self.logger.info('Country name not found. Creating one now. Preloading country list with ISO codes is reccommended.')
                country.name = country_name
                country.iso_code = country_iso_code
                session.add(country)
                session.flush()

                # Get the id for the new country description. Will be stored in the geological table.
                country_id = country.id
                self.logger.info('New country id: {}'.format(country.id))
                session.commit()

            return country_id

        except Exception as e:
            session.rollback()
            raise e

    def insert_state(self, body, session, country_id):

        try:
            #### Insert State info ####
            self.logger.info('Checking state information')
            state = State()
            state_id = None

            ### 1 ### Fill N/A in for none
            if body['location']['state']:
                state_name = body['location']['state'].title()
            else:
                state_name = 'None'

            ### 2 ### Check if state description exists and create one if not.
            state_id = session.query(State.id)\
                          .filter(and_(State.name == state_name,\
                                        State.id_country == country_id))\
                          .one_or_none()

            if state_id:

                state_id = state_id[0]
                self.logger.info('State description was found: {}'.format(state_id))
                # The next line is a hack-fix. For some reason the query returns a tuple but
                # not just the value - if there is a result is should only be one and integer.
            else:
                # Description does not exist so it will be created in db.
                self.logger.info('State description not found. New state will be created.')
                state.id_country = country_id
                state.name = state_name
                session.add(state)
                session.flush()

                # Get the id for the new state description. Will be stored in the station table.
                state_id = state.id
                self.logger.info('State id: {}'.format(state.id))
                session.commit()

            return state_id

        except Exception as e:
            session.rollback()
            raise e

    def insert_city(self, body, session, state_id):

        try:
            #### Insert City info ####
            self.logger.info('Checking city information')
            city = City()
            city_id = None

            ### 1 ### Fill N/A in for none
            if body['location']['city']:
                city_name = body['location']['city'].title()
            else:
                city_name = 'None'

            ### 2 ### Check if state description exists and create one if not.
            city_id = session.query(City.id)\
                          .filter(City.name == city_name)\
                          .one_or_none()

            if city_id:

                city_id = city_id[0]
                self.logger.info('City description was found: {}'.format(city_id))
                # The next line is a hack-fix. For some reason the query returns a tuple but
                # not just the value - if there is a result is should only be one and integer.
            else:
                # Description does not exist so it will be created in db.
                self.logger.info('City description not found. New city will be created.')
                city.id_state = state_id
                city.name = city_name
                session.add(city)
                session.flush()

                # Get the id for the new state description. Will be stored in the station table.
                city_id = city.id
                self.logger.info('City id: {}'.format(city.id))
                session.commit()

            return city_id

        except Exception as e:
            session.rollback()
            raise e

    def insert_tectonic(self, body, session):

        try:
            #### Insert Tectonic info ####
            self.logger.info('Checking tectonic information.')
            tectonic = Tectonic()
            tectonic_id = None

            ### 1 ### Fill N/A in for none
            if body['location']['tectonic']['plate_name']:
                tectonic_plate_name = body['location']['tectonic']['plate_name'].upper()
            else:
                tectonic_plate_name = 'None'

            ### 2 ### Check if country description exists and create one if not.
            tectonic_plate_id = session.query(Tectonic.id)\
                              .filter(Tectonic.plate_name == tectonic_plate_name)\
                              .one_or_none()

            if tectonic_plate_id:
                self.logger.info('Tectonic plate was found: {}'.format(tectonic_plate_name))
                # The next line is a hack-fix. For some reason the query returns a tuple but
                # not just the value - if there is a result is should only be one and integer.

            else:
                # Description does not exist so it will be created in db.
                self.logger.info('Tectonic plate was not found. Creating one now.')
                tectonic.plate_name = tectonic_plate_name
                session.add(tectonic)
                session.flush()

                # Get the id for the new tectonic_plate description. Will be stored in the geological table.
                tectonic_plate_id = tectonic.id
                self.logger.info('New tectonic_plate id: {}'.format(tectonic.id))
                session.commit()

            return tectonic_plate_id

        except Exception as e:
            session.rollback()
            raise e

    def insert_monument(self, body, session):

        try:
            #### Insert Tectonic info ####
            self.logger.info('Checking monument information.')
            monument = Monument()
            monument_id = None

            ### 1 ### Fill N/A in for none
            if body['monument']['description']:
                monument_description = body['monument']['description']
            else:
                monument_description = 'None'


            # TODO: Check if this field is used in Sitelog
            #if body['monument']['inscription']:
            #    monument_inscription = body['monument']['inscription']
            #else:
            #    monument_inscription = body['monument']['inscription']

            if body['monument']['foundation']:
                monument_foundation = body['monument']['foundation']
            else:
                monument_foundation = 'None'

            if body['monument']['height']:
                monument_height = body['monument']['height']
            else:
                monument_height = 0.00

            if body['monument']['foundation_depth']:
                monument_foundation_depth = body['monument']['foundation_depth']
            else:
                monument_foundation_depth = 0.00                

            self.logger.info('monument: {} {}'.format(type(monument_foundation_depth), type(monument_height)))

            ### 2 ### Check if country description exists and create one if not.
            monument_id = session.query(Monument.id)\
                              .filter(and_(Monument.description == monument_description,
                                        Monument.height == monument_height,
                                        Monument.foundation == monument_foundation,
                                        Monument.foundation_depth == monument_foundation_depth))\
                              .one_or_none()

            if monument_id:
                monument_id = monument_id[0]
                self.logger.info('Monument plate was found: {}'.format(monument_id))
                # The next line is a hack-fix. For some reason the query returns a tuple but
                # not just the value - if there is a result is should only be one and integer.

            else:
                # Description does not exist so it will be created in db.
                self.logger.info('Monument plate was not found. Creating one now.')
                monument.description = monument_description
                monument.foundation = monument_foundation
                monument.foundation_depth = monument_foundation_depth
                monument.height = monument_height
                session.add(monument)
                session.flush()

                # Get the id for the new monument description. Will be stored in the geological table.
                monument_id = monument.id
                self.logger.info('New monument id: {}'.format(monument.id))
                session.commit()

            return monument_id

        except Exception as e:
            session.rollback()
            raise e

    def insert_coordinates(self, body, session):

        #try:
            #### Insert coordinates info ####
            self.logger.info('Checking coordinates information.')
            coordinates = Coordinates()
            coordinates_id = None

            ### 1 ### Fill N/A in for none
            if body['location']['coordinates']['X']:
                coordinates_x = body['location']['coordinates']['X']
            else:
                coordinates_x = 'None'

            if body['location']['coordinates']['Y']:
                coordinates_y = body['location']['coordinates']['Y']
            else:
                coordinates_y = 'None'

            if body['location']['coordinates']['Z']:
                coordinates_z = body['location']['coordinates']['Z']
            else:
                coordinates_z = 'None'

            if body['location']['coordinates']['lat']:
                coordinates_lat = body['location']['coordinates']['lat']
            else:
                coordinates_lat = 'None'

            if body['location']['coordinates']['lon']:
                coordinates_lon = body['location']['coordinates']['lon']
            else:
                coordinates_lon = 'None'

            if body['location']['coordinates']['altitude']:
                coordinates_altitude = body['location']['coordinates']['altitude']
            else:
                coordinates_altitude = 'None'

            ### 2 ### Check if country description exists and create one if not.
            coordinates_id = session.query(Coordinates.id)\
                              .filter(and_(Coordinates.x == coordinates_x,
                                            Coordinates.y == coordinates_y,
                                            Coordinates.z == coordinates_z,
                                            Coordinates.lat == coordinates_lat,
                                            Coordinates.lon == coordinates_lon,
                                            Coordinates.altitude == coordinates_altitude))\
                              .one_or_none()

            if coordinates_id:
                coordinates_id = coordinates_id[0]
                self.logger.info('Coordinate description was found: {}'.format(coordinates_id))

            else:
                # Description does not exist so it will be created in db.
                self.logger.info('Coordinate description was not found. Creating one now.')
                coordinates.x = coordinates_x
                coordinates.y = coordinates_y
                coordinates.z = coordinates_z
                coordinates.lat = coordinates_lat
                coordinates.lon = coordinates_lon
                coordinates.altitude = coordinates_altitude

                session.add(coordinates)
                session.flush()

                # Get the id for the new tectonic_plate description. Will be stored in the geological table.
                coordinates_id = coordinates.id
                self.logger.info('New coordinates id: {}'.format(coordinates.id))
                session.commit()

            return coordinates_id

        #except Exception as e:
        #    session.rollback()
        #    raise e

    def insert_location(self, body, session, tectonic_id, city_id, coordinates_id):

        try:
            #### Insert Location info ####
            self.logger.info('Checking location information.')
            location = Location()
            location_id = None

            ### 1 ### Fill N/A in for none
            if body['location']['description']:
                location_description = body['location']['description']
            else:
                location_description = 'None'

            ### 2 ### Check if country description exists and create one if not.
            location_id = session.query(Location.id)\
                                    .filter(and_(Location.description == location_description,\
                                                 Location.id_city == city_id,\
                                                 Location.id_tectonic == tectonic_id,\
                                                 Location.id_coordinates == coordinates_id))\
                                    .one_or_none()

            if location_id:
                self.logger.info('Location was found: {}'.format(location_description))
            else:
                # Description does not exist so it will be created in db.
                self.logger.info('Location was not found. Creating one now.')
                
                location.description = location_description
                location.id_city = city_id
                location.id_tectonic = tectonic_id
                location.id_coordinates = coordinates_id
               
                session.add(location)
                session.flush()

                # Get the id for the new location description. Will be stored in the geological table.
                location_id = location.id
                self.logger.info('New location id: {}'.format(location.id))
                session.commit()

            return location_id

        except Exception as e:
            session.rollback()
            raise e

    def insert_station_type(self, body, session):

        try:
            #### Insert Station type info ####
            self.logger.info('Checking station type information.')
            station_type = StationType()
            station_type_id = None

            ### 1 ### Fill N/A in for none
            # TODO: value missing from sitelog
            #if body['station_type']:
            #    station_type_name = body['station_type']
            #else:
            #    station_type_name = 'None'

            
            if body['station_type']:
                station_type_type = body['station_type']
            else:
                station_type_type = 'None'

            ### 2 ### Check if country description exists and create one if not.
            station_type_id = session.query(StationType.id)\
                               .filter(StationType.type == station_type_type)\
                              .one_or_none()

            if station_type_id:
                self.logger.info('Station type was found: {}'.format(station_type_type))
                # The next line is a hack-fix. For some reason the query returns a tuple but
                # not just the value - if there is a result is should only be one and integer.

            else:
                # Description does not exist so it will be created in db.
                self.logger.info('Station type plate was not found. Creating one now.')
                station_type.name = 'None'
                station_type.type = station_type_type
                session.add(station_type)
                session.flush()

                # Get the id for the new station_type description. Will be stored in the geological table.
                station_type_id = station_type.id
                self.logger.info('New station_type id: {}'.format(station_type.id))
                session.commit()

            return station_type_id

        except Exception as e:
            session.rollback()
            raise e

    def insert_agency(self, contact, session):

        #self.logger.info('contact dict: {}'.format(contact))
        #self.logger.info('agency dict: {}'.format(contact['contact']['agency']))

        try:
            #### Insert Agency info ####
            self.logger.info('Checking agency information.')
            agency = Agency()
            agency_id = None

            ### 1 ### Fill N/A in for none
            if contact['contact']['agency']['name']:
                agency_name = contact['contact']['agency']['name']
            else:
                agency_name = 'None'

            if contact['contact']['agency']['abbreviation']:
                agency_abbreviation = contact['contact']['agency']['abbreviation']
            else:
                agency_abbreviation = 'None'

            if contact['contact']['agency']['address']:
                agency_address = contact['contact']['agency']['address']
            else:
                agency_address = 'None'

            if contact['contact']['agency']['www']:
                agency_www = contact['contact']['agency']['www']
            else:
                agency_www = 'None'

            if contact['contact']['agency']['infos']:
                agency_infos = contact['contact']['agency']['infos']
            else:
                agency_infos = 'None'


            ### 2 ### Check if country description exists and create one if not.
            agency_id = session.query(Agency.id)\
                              .filter(and_(Agency.name == agency_name,
                                            Agency.abbreviation == agency_abbreviation,
                                            Agency.address == agency_address,
                                            Agency.www == agency_www))\
                              .one_or_none()

            if agency_id:
                agency_id = agency_id[0]
                self.logger.info('Agency description was found: {}'.format(agency_id))
                # The next line is a hack-fix. For some reason the query returns a tuple but
                # not just the value - if there is a result is should only be one and integer.

            else:
                # Description does not exist so it will be created in db.
                self.logger.info('Agency name not found. Creating one now.')
                agency.name = agency_name
                agency.abbreviation = agency_abbreviation
                agency.address = agency_address
                agency.www = agency_www
                agency.infos = agency_infos

                session.add(agency)
                session.flush()

                # Get the id for the new country description. Will be stored in the geological table.
                agency_id = agency.id
                self.logger.info('New agency id: {}'.format(agency.id))
                session.commit()

            #return agency_id
            return agency_id

        except Exception as e:
            session.rollback()
            raise e

    def insert_contact(self, station_contact, session, agency_id):
 
        try:
            #### Insert Contact info ####
            self.logger.info('Checking contact information')
            contact = Contact()
            contact_id = None

            ### 1 ### Fill N/A in for none
            if station_contact['contact']['name']:
                contact_name = station_contact['contact']['name']
            else:
                contact_name = 'None'

            if station_contact['contact']['title']:
                contact_title = station_contact['contact']['title']
            else:
                contact_title = 'None'

            if station_contact['contact']['email']:
                contact_email = station_contact['contact']['email']
            else:
                contact_email = 'None'

            if station_contact['contact']['phone']:
                contact_phone = station_contact['contact']['phone']
            else:
                contact_phone = 'None'

            # TODO: gsm missing from sitelog/JSON. Contact Jean-Luc for a fix            
            #if station_contact['gsm']:
            #    contact_gsm = station_contact['gsm']
            #else:
            #    contact_gsm = 'None'
            contact_gsm = 'None'

            if station_contact['contact']['comment']:
                contact_comment = station_contact['contact']['comment']
            else:
                contact_comment = 'None'

            #TODO: move role under contact_comment
            if station_contact['contact']['role']:
                contact_role = station_contact['contact']['role']
            else:
                contact_role = 'None'
            #contact_role = 'None'

            #self.logger.info('Contact; {}{}{}'.format(contact_name, contact_email, contact_comment))

            ### 2 ### Check if state description exists and create one if not.
            contact_id = session.query(Contact.id)\
                          .filter(and_(Contact.name == contact_name,
                                        Contact.email == contact_email))\
                          .one_or_none()

            if contact_id:
                contact_id = contact_id[0]
                self.logger.info('Contact description was found: {}'.format(contact_id))
                # The next line is a hack-fix. For some reason the query returns a tuple but
                # not just the value - if there is a result is should only be one and integer.
            else:
                # Description does not exist so it will be created in db.
                self.logger.info('Contact description not found. New contact will be created.')
                contact.id_agency = agency_id
                contact.name = contact_name
                contact.title = contact_title
                contact.email = contact_email
                contact.phone = contact_phone
                contact.gsm = contact_gsm
                contact.comment = contact_comment
                contact.role = contact_role
                session.add(contact)
                session.flush()

                # Get the id for the new state description. Will be stored in the station table.
                contact_id = contact.id
                self.logger.info('Contact id: {}'.format(contact.id))
                session.commit()

            return contact_id

        except Exception as e:
            session.rollback()
            raise e

    def insert_network(self, body, session, contact_id):

        network_id_list = []

        for a_network in body['station_networks']:

            try:
                #### Insert Network info ####
                self.logger.info('Checking network information')
                network = Network()
                network_id = None

                ### 1 ### Fill N/A in for none
                if a_network['name']:
                    network_name = a_network['name']
                else:
                    network_name = 'None'


                ### 2 ### Check if state description exists and create one if not.
                network_id = session.query(Network.id)\
                              .filter(Network.name == network_name)\
                              .one_or_none()

                if network_id:

                    network_id = network_id[0]
                    self.logger.info('Network description was found: {}'.format(network_id))
                    # The next line is a hack-fix. For some reason the query returns a tuple but
                    # not just the value - if there is a result is should only be one and integer.
                else:
                    # Description does not exist so it will be created in db.
                    self.logger.info('Network description not found. New netowrk will be created.')
                    network.name = network_name
                    #TODO: See how to handle contact_id-less network info...
                    #network.id_contact = contact_id
                    session.add(network)
                    session.flush()

                    # Get the id for the new state description. Will be stored in the station table.
                    network_id = network.id

                    self.logger.info('Network id: {}'.format(network.id))
                    session.commit()

                network_id_list.append(network_id)

            except Exception as e:
                session.rollback()
                raise e

        return network_id_list

    def insert_station(self, body, session, station_type_id, location_id, geological_id, monument_id):

        try:
            #### Finally insert Station info ####
            self.logger.info('Creating station object..')

            station = Station()
            station.name = body['name']
            station.marker = body['marker'].upper()
            station.description = body['description']
            station.date_from = body['date_from']
            station.date_to = body['date_to']
            station.iers_domes = body['iers_domes']
            station.cdp_num = body['cdp_num']
            station.antenna_num = body['antenna_num']
            station.receiver_num = body['receiver_num']
            station.country = body['country']
            station.comment = body['comment']
            station.id_geological = geological_id
            station.id_location = location_id
            station.id_monument = monument_id
            station.id_station_type = station_type_id

            session.add(station)
            session.flush()

            station_marker = station.marker
            station_id = station.id

            self.logger.info('ID for new station is: {}'.format(station_id))
            session.commit()

            return station_marker, station_id

        except Exception as e:
            session.rollback()
            raise e

    def insert_station_contact(self, session, station_id, contact_id_list):

        station_contact_id_list = []

        for contact_id in contact_id_list:

            self.logger.info('station_id: {} contact_id: {}'.format(station_id, contact_id))

            try:
                #### Insert City info ####
                self.logger.info('Checking station_contact relationship information')
                station_contact = StationContact()
                station_contact_id = None

                ### 2 ### Check if state description exists and create one if not.
                station_contact_id = session.query(StationContact.id)\
                              .filter(and_(StationContact.id_station == station_id,
                                            StationContact.id_contact == contact_id))\
                              .one_or_none()

                if station_contact_id:

                    self.logger.info('station_contact relationship description was found: {}'.format(station_contact_id))

                else:
                    # Description does not exist so it will be created in db.
                    self.logger.info('station_contact relationship not found. New relationship will be created.')
                    station_contact.id_station = station_id
                    station_contact.id_contact = contact_id
                    session.add(station_contact)
                    session.flush()

                    # Get the id for the new state description. Will be stored in the station table.
                    station_contact_id = station_contact.id
                    self.logger.info('station_contact id: {}'.format(station_contact_id))
                    session.commit()

                station_contact_id_list.append(station_contact_id)

            except Exception as e:
                session.rollback()
                raise e

        return station_contact_id_list

    def insert_station_network(self, session, station_id, network_id_list):

        station_network_id_list = []

        for network_id in network_id_list:

            self.logger.info('station_id: {} network_id: {}'.format(station_id, network_id))

            try:
                #### Insert City info ####
                self.logger.info('Checking station_network relationship information')
                station_network = StationNetwork()
                station_network_id = None

                ### 2 ### Check if state description exists and create one if not.
                station_network_id = session.query(StationNetwork.id)\
                              .filter(and_(StationNetwork.id_station == station_id,
                                            StationNetwork.id_network == network_id))\
                              .one_or_none()

                if station_network_id:

                    self.logger.info('station_network relationship description was found: {}'.format(station_network_id))

                else:
                    # Description does not exist so it will be created in db.
                    self.logger.info('station_network relationship not found. New station_network relationship will be created.')
                    station_network.id_station = station_id
                    station_network.id_network = network_id
                    session.add(station_network)
                    session.flush()

                    # Get the id for the new state description. Will be stored in the station table.
                    station_network_id = station_network.id
                    self.logger.info('station_network id: {}'.format(station_network_id))
                    session.commit()

                station_network_id_list.append(station_network_id)

            except Exception as e:
                session.rollback()
                raise e

        return station_network_id_list

    def insert_local_tie(self, body, session, station_id):

        local_tie_list = []

        for a_local_tie in body['local_ties']:
            try:
                #### Insert Local tie info ####
                self.logger.info('Checking state information')
                local_tie = LocalTie()
                local_tie_id = None

                ### 1 ### Fill N/A in for none
                if a_local_tie['name']:
                    local_tie_name = a_local_tie['name']
                else:
                    local_tie_name = 'None'

                if a_local_tie['usage']:
                    local_tie_usage = a_local_tie['usage']
                else:
                    local_tie_usage = 'None'

                if a_local_tie['cpd_num']:
                    local_tie_cpd_num = a_local_tie['cpd_num']
                else:
                    local_tie_cpd_num = 'None'

                if a_local_tie['dx']:
                    local_tie_dx = a_local_tie['dx']
                else:
                    local_tie_dx = 0

                if a_local_tie['dy']:
                    local_tie_dy = a_local_tie['dy']
                else:
                    local_tie_dy = 0

                if a_local_tie['dz']:
                    local_tie_dz = a_local_tie['dz']
                else:
                    local_tie_dz = 0

                if a_local_tie['accuracy']:
                    local_tie_accuracy = a_local_tie['accuracy']
                else:
                    local_tie_accuracy = 0

                if a_local_tie['survey_method']:
                    local_tie_survey_method = a_local_tie['survey_method']
                else:
                    local_tie_survey_method = 'None'

                if a_local_tie['date_at']:
                    local_tie_date_at = a_local_tie['date_at']
                else:
                    local_tie_date_at = '9999-01-01 00:00:00'

                if a_local_tie['comment']:
                    local_tie_comment = a_local_tie['comment']
                else:
                    local_tie_comment = 'None'


                ### 2 ### Check if state description exists and create one if not.
                local_tie_id = session.query(LocalTie.id)\
                              .filter(and_(LocalTie.name == local_tie_name,
                                        LocalTie.usage == local_tie_usage,
                                        LocalTie.cpd_num == local_tie_cpd_num,
                                        LocalTie.dx == local_tie_dx,
                                        LocalTie.dy == local_tie_dy,
                                        LocalTie.dz == local_tie_dz,
                                        LocalTie.accuracy == local_tie_accuracy,
                                        LocalTie.survey_method == local_tie_survey_method,
                                        LocalTie.date_at == local_tie_date_at,
                                        LocalTie.comment == local_tie_comment))\
                              .one_or_none()

                if local_tie_id:

                    local_tie_id = local_tie_id[0]
                    self.logger.info('Local tie description was found: {}'.format(local_tie_id))
                    # The next line is a hack-fix. For some reason the query returns a tuple but
                    # not just the value - if there is a result is should only be one and integer.
                else:
                    # Description does not exist so it will be created in db.
                    self.logger.info('Local tie description not found. New state will be created.')
                    local_tie.id_station = station_id
                    local_tie.name = local_tie_name
                    local_tie.usage = local_tie_usage
                    local_tie.cpd_num = local_tie_cpd_num
                    local_tie.dx = local_tie_dx
                    local_tie.dy = local_tie_dy
                    local_tie.dz = local_tie_dz
                    local_tie.accuracy = local_tie_accuracy
                    local_tie.survey_method = local_tie_survey_method
                    local_tie.date_at = local_tie_date_at
                    local_tie.comment = local_tie_comment

                    session.add(local_tie)
                    session.flush()

                    # Get the id for the new local tie description. Will be stored in the station table.
                    local_tie_id = local_tie.id
                    self.logger.info('Local tie id: {}'.format(local_tie.id))
                    session.commit()

                local_tie_list.append(local_tie_id)

            except Exception as e:
                session.rollback()
                raise e

        return local_tie_list

    def insert_instrument_collocation(self, body, session, station_id):

        instrument_collocation_list = []
        #self.logger.info('Instrument colocation list: {}'.format(body['instrument_collocation']))

        for instrument in body['instrument_collocation']:
            try:
                #### Insert Colocated Instrument info ####
                self.logger.info('Checking instrument colocation information')
                instrument_collocation = InstrumentColocation()
                instrument_collocation_id = None

                ### 1 ### Fill N/A in for none
                if instrument['type']:
                    instrument_collocation_type = instrument['type']
                else:
                    instrument_collocation_type = 'None'

                if instrument['status']:
                    instrument_collocation_status = instrument['status']
                else:
                    instrument_collocation_status = 'None'

                if instrument['date_from']:
                    instrument_collocation_date_from = instrument['date_from']
                else:
                    instrument_collocation_date_from = '9999-01-01 00:00:00'

                if instrument['date_to']:
                    instrument_collocation_date_to = instrument['date_to']
                else:
                    instrument_collocation_date_to = '9999-01-01 00:00:00'

                if instrument['comment']:
                    instrument_collocation_comment = instrument['comment']
                else:
                    instrument_collocation_comment = 'None'


                ### 2 ### Check if state description exists and create one if not.
                instrument_collocation_id = session.query(InstrumentColocation.id)\
                              .filter(and_(InstrumentColocation.type == instrument_collocation_type,
                                        InstrumentColocation.status == instrument_collocation_status,
                                        InstrumentColocation.date_from == instrument_collocation_date_from,
                                        InstrumentColocation.date_to == instrument_collocation_date_to))\
                              .one_or_none()

                if instrument_collocation_id:

                    instrument_collocation_id = instrument_collocation_id[0]
                    self.logger.info('Instrument colocation description was found: {}'.format(instrument_collocation_id))
                    # The next line is a hack-fix. For some reason the query returns a tuple but
                    # not just the value - if there is a result is should only be one and integer.
                else:
                    # Description does not exist so it will be created in db.
                    self.logger.info('Instrument colocation description not found. New state will be created.')
                    instrument_collocation.id_station = station_id
                    instrument_collocation.type = instrument_collocation_type
                    instrument_collocation.status = instrument_collocation_status
                    instrument_collocation.date_from = instrument_collocation_date_from
                    instrument_collocation.date_to = instrument_collocation_date_to
                    instrument_collocation.comment = instrument_collocation_comment

                    session.add(instrument_collocation)
                    session.flush()

                    # Get the id for the new local tie description. Will be stored in the station table.
                    instrument_collocation_id = instrument_collocation.id
                    self.logger.info('Instrument colocation id: {}'.format(instrument_collocation.id))
                    session.commit()

                instrument_collocation_list.append(instrument_collocation_id)

            except Exception as e:
                session.rollback()
                raise e

        return instrument_collocation_list

def main():
    '''
    This main function of the class is mainly for testing purposes.
    It runs all the queries to see if all of them run as expected.
    '''

    print " >> This is a test run for the Queries() module"
    print " >> ..."

    # We must provide the Queries class database information for connection
    print " >> Defining database conneciton"
    db_info = {'db_type':'postgresql', 'user':'gps', 'password':'gps',
               'hostname':'localhost', 'db_name':'gps'}

    # Then we instantiate the class
    print " >> Instantiating the Queries() class "
    queries = Queries()

    # Test if data is in the data model.
    # The test will fail if data is missing
    #print " >> Test 1: Data in datamodel"
    #queries.datamodel_check()

    # Test /station
    #print " "
    #print " >> Test 2: Query '/station'"
    #print " "
    #args = {'bbox':None, 'llrad':None, 'dates':None, 'item_type':None, 'model':None}
    #station_list, status_code = queries.station_list(args)

    #parsed_list = json.loads(station_list)
    #print json.dumps(parsed_list, indent=3, sort_keys=True)

    # Test /station/{marker}
    print " "
    print " >> Test 3: Query '/station/{marker}'"
    print " "
    station_single, status_code = queries.station_single('710')

    parsed_single = json.loads(station_single)
    print json.dumps(parsed_single, indent=3, sort_keys=True)

if __name__ == "__main__":
    main()
