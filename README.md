# 1. Introduction #

This document describes the usage and adaptation of a Web Services Server being developed as part of EPOS-IP WP10 by Icelandic Meteorological Office. 
The server is based on the FLASK python framework and includes an application programming interface (API) for a GPS database that has been co-developed by IMO and WP10. 
This API handles the querying via REST endpoints. The solutions design is lightweight, meaning it has been written, for quick deployment, testing and evaluation.
**The server is meant for test, development and internal use only. For operational purposes the server needs to be reinforced**
The software and this accompanying manual are work in progress and contain bugs, errors and omissions.


**Author:** Fjalar Sigurðarson (fjalar@vedur.is)

**Date created:** 10. February 2017
**Date last update:** 21. June 2017

## 1.1 Currently supported database schema ##
https://gitlab.com/gpseurope/database/tags/v0.2.7


## 1.2 Files ##
The following files are contained in the archive:

    /examples                                       Few sitelog examples and other JSON examples for service inputs.
    /indexer                                        Rinex indexing script and samples.
    /postman/GNSS Europe.postman_collection.json    Postman service definition file for API testing see https://www.getpostman.com/ for more information.
    /swagger                                        Swagger yaml file for Service documentation and install instructions.
    README.md		                                This readme file you are reading.
    datamodels.py					                SQLAlchemy data models to match the GPS database.
    import_sitelogs.py                              A script that reads JSON sitelogs from a folder and loads them to the /gps/sitelog POST service
    queries.py			                            Queries for each API call working on the SQLAlchemy data models.
    web_server.cfg                                  Config file for the server. 
    web_server.py	                                The web server. 

# 2. Installation #
## 2.1 Python ##

You need Python 2.7 and the Python modules **flask**, **sqlalchemy** and **requests** installed.

    https://www.python.org/downloads

	http://flask.pocoo.org/docs/0.11/installation

	http://www.sqlalchemy.org
	
	https://pypi.python.org/pypi/requests/2.11.1
	
Python 3.x will not be supported without some refactoring of the code.

## 2.2 Database ##
We use PostgreSQL so you need to download it and install it. Here is a place to start https://www.postgresql.org/download/

When your database service is up and running you need to import the database schema and support data from the database dump file.
The scripts assume the database is 'gps' and the user is named 'gps' with owner privileges. 

Create a database 'gps':

	$ createdb -h <hostname> -U gps -W gps

Create a user 'gps':

	$ createuser -h <hostname> gps

Import the dump with:

	$ psql -h <hostname> -U gps -d gps -W < gps.<date>.postgres.dump

## 2.3 Web server base code ##

**1)** In a terminal, clone the FWSS project from the gitlab.com to a folder

    git@gitlab.com:gpseurope/flaks-web-service-server-2.git fwss

**2)** Open a terminal window, and change into the **fwss** directory.

## 2.4 Modify web_server.cfg ##
Change username, password, hostname and dbname to what was used in 2.2 Database.

    [Server]

    # The debug switch is 'True' or 'False' to enable/disable debug messaging from the server.
     app_debug: True

    # To enable access to the server via the intranet, enable the following line.
     #app_run: 0.0.0.0

    # To connect to a certain database, basic information must be provided. Example is given here below.
    #db_type: postgresql
    #username: gps 
    #password: gps
    #hostname: localhost 
    #db_name: gps
    

## 2.5 Run the server ##

Run the flask server with:

    $ python web_server.py

To monitor the logging output from the queries.py module in python it can be useful to run the following command in a separate terminal:

    $ touch queries.log && tail -f queries.log

## 2.6 Import Sitelogs in JSON format ##

**1)** Create a folder called sitelog in the web server folder

**2)** Copy Sitelogs in JSON format to the folder.

**3)** Run then the following script:

    $ python import_sitelog.py
    
**4)** Or, modify the import_sitelog.py to point to a sitelog directory currently on the machine.


# 3 Current development #

## 3.1 Load tables for Rinex indexing ##
This is just a work-around while tables have not been pre-loaded.

Tables **data_center** and **file_type** need to contain some records before rinex files can be indexed. 

While these tables have not been pre-loaded (but probably will) you can use the following steps:


**1)** Insert a data center into the data_center table

	$ psql -U gps gnss-europe-v0-2-7

	$ insert into data_center (acronym, id_agency, hostname, access_conditions, name, url) values ('DC1',1,'dc1.com','ftp','DC1','http://dc1.com'); 
	$ insert into data_center (acronym, id_agency, hostname, access_conditions, name, url) values ('IMO',1,'vedur.is','nfs','Icelandic Meteorological Office','http://vedur.is');

	# These entries are currently only for testing purposes

**2)** Insert a file_type into the file_type table

	$ psql -U gps gnss-europe-v0-2-7

	$> insert into file_type (format, sampling_window, sampling_frequency) values ('N/A','N/A','N/A');
	$> insert into file_type (format, sampling_window, sampling_frequency) values ('RINEX2','24hr','30s');

	# These entries are currently only for testing purposes

## 3.2 To reset the database schema and/or load a new one
Mind to adjust the db numbering (vx-y-z)

**1)** Connect

	$ psql -U gps gnss-europe-v0-2-7

**2)** Wipe current db clean

	$> \c sub; 
	$> drop database "gnss-europe-v0-2-7"; drop schema "gnss-europe-v0-2-7";create schema "gnss-europe-v0-2-7"; create database "gnss-europe-v0-2-7"; 
	$> \c "gnss-europe-v0-2-7";
	$> \q (exit postgresql shell)

**3)** Load the schema to the db (current or new, mind the numbering)

	$ psql -U gps gnss-europe-v0-2-7 < gnss-europe.pgdump.sql

**4)** Load some sitelogs

	$ python import_sitelogs.py (and have some sitelogs in the sitelog folder)

# 4. Querying #

Postman (https://www.getpostman.com) is a convenient client for querying the REST endpoints. in the /postman folder, the file GNSS Europe.postman_collection.json contains example queries that can be imported to Postman.
Alternatively, you can use the python requests package and write your queries by hand, or you can use the curl command (Postman is also capable of generating code for this).

Swagger (http://swagger.io) is a convenient way to provide a documentation with testing capabilities to web services. In the /swagger folder, 

Following are a few example queries.


## 4.1 GET /gps/station ##

**Result:** returns JSON array of all stations

Example response:

    [
      {
            "station_networks": [
                {
                    "network": {
                        "name": "A"
                    }
                }
            ],
            "name": "Salamanca",
            "date_to": null,
            "marker": "SALA",
            "date_from": "2006-04-25 00:00:00",
            "agency": {
                "name": "Instituto Geografico Nacional"
            },
            "id": 1,
            "location": {
                "country": {
                    "name": "Spain"
                },
                "state": {
                    "name": "Salamanca"
                },
                "coordinates": {
                    "lat": 40.9451,
                    "altitude": 855.4,
                    "lon": -5.4959
                },
                "city": {
                    "name": "Salamanca"
                }
            }
        },
        {
        "station_networks": [
            {
                "network": {
                    "name": "A"
                }
            }
        ],
        "name": "Vacov",
        "date_to": null,
        "marker": "VACO",
        "date_from": "2004-10-20 12:00:00",
        "agency": {
            "name": "Academy of Science of the Czech Republic Institute of Rock Structure and Mechanics Department of Geofactors"
        },
        "id": 2,
        "location": {
            "country": {
                "name": "Czech Republic"
            },
            "state": {
                "name": "Strakonice Region"
            },
            "coordinates": {
                "lat": 49.1338,
                "altitude": 799.4,
                "lon": 13.7242
            },
            "city": {
                "name": "Vacov"
            }
        }
    ]

	

CURL:

	$ curl -X GET -H "Content-Type: application/json" -H "Cache-Control: no-cache" http://localhost:5000/gps/station

Python:

	import requests

	url = "http://localhost:5000/gps/station"

	payload = ""
	headers = {
	    'content-type': "application/json",
	    'cache-control': "no-cache",
	    'postman-token': "86165692-33c5-55f7-9992-1ba0f5aad16e"
	    }

	response = requests.request("GET", url, data=payload, headers=headers)

	print(response.text)


## 4.2 GET /gps/station/{marker} ##

**Result:** returns JSON array of station with {marker}

Example response for marker = 'SALA':

      {
            "station_networks": [
                {
                    "network": {
                        "name": "A"
                    }
                }
            ],
            "name": "Salamanca",
            "date_to": null,
            "marker": "SALA",
            "date_from": "2006-04-25 00:00:00",
            "agency": {
                "name": "Instituto Geografico Nacional"
            },
            "id": 1,
            "location": {
                "country": {
                    "name": "Spain"
                },
                "state": {
                    "name": "Salamanca"
                },
                "coordinates": {
                    "lat": 40.9451,
                    "altitude": 855.4,
                    "lon": -5.4959
                },
                "city": {
                    "name": "Salamanca"
                }
            }
        }


## 4.3 POST /gps/sitelog ##

**Result:** database insert of station JSON array in request BODY

Example body for sitelog is a bit too big but, the first line look like this:

{
    "antenna_num": 0,
    "cdp_num": null,
    "collocation_instruments": [],
    "comment": "BCLN is a reference point of the National Reference Stations Network raised by the Inst. Geografico Nacional. The monument is a reinforced concrete pillar on the top of the terrace of the Conservation Roads' Center of Sans Vicens dels Horts (Barcelona)",
    "conditions": [],
    "country": "Spain",
    "date_from": "2012-01-31 00:00:00",
    "date_to": null,
    "description": "BRASS PLATE",
    "geological": {
        "bedrock": {
            "condition": "WEATHERED",
            "type": "SEDIMENTARY"
        },
        "characteristic": "CLAY/LIMESTONE/CLAY",
        "distance_to_fault": null,
        "fault_zone": "NO",
        "fracture_spacing": null
    },

Example response:

	200 OK New station BCLN was created with id = 684

## 4.4 POST /gps/rinex ##

**Result:** database insert of rinex file JSON information.

Example query:

    {
      "station_marker": "GFUM",
      "file_name": "GFUM1423.17D.Z",
      "full_url":"gpsops@rek:/data/2017/may/GFUM/15s_24hr/rinex/GFUM1420.17D.Z",
      "local_path": "/data/2017/may/GFUM/15s_24hr/rinex/GFUM1420.17D.Z",
      "data_center": "IMO",
      "reference_date":"2017-06-10 00:00:00",
      "published_date":"2017-06-11 00:01:34",
      "file_size": "12345",
      "md5_checksum": "474ecf5514f1d2f7a009964c549e1af4"
    }


Example response:
    
    201 CREATED Entry was added with id = 1


# 5. Known issues / bugs #

1) Results for /gps/station/{marker} can give two results for the same station. This is becuase contacts are not properly prioritized as primary and secondary in the Sitelogs resulting in multiple outputs.